# Signature detection using YOLOv5
## You Only Look Once v5 (YOLOv5)
![Yolo comparison](../../Images/yolo_comparison.png)
YOLOv5 is a state of the art object-detection algorithms thats being widely used in academia and industry alike. It is latest version of the versatile and powerful object detection algorithm called YOLO. It outperforms all other real-time object detection models in the world.  


### Working of YOLO
![YOLO Working](../../Images/yolo_working.jpeg)
YOLO uses convolutional neural networks instead of the region-based methods employed by alogirithms like R-CNN. The convolutional network *Only Look Once*, ie it requires only one forward pass through the neural network to make predictions. It makes two predictions, the class of object and the position of the objects present in the image.  
YOLO divides an image into `nxn` regions and predicts whether a target class is present in each region or not. It also predicts the bounding box coordinates of the target class. [Non-max suppression](https://towardsdatascience.com/non-maximum-suppression-nms-93ce178e177c) is used to prevent same object being detected multiple times.  
Refer [this video](https://www.youtube.com/watch?v=MhftoBaoZpg) for a better, in-depth understanding of YOLO models. The original YOLO paper could be accessed [here](https://arxiv.org/abs/1506.02640) and YOLOv5 repo could be found [here](https://github.com/ultralytics/yolov5)


### Steps to get yolov5 trained model
git clone https://github.com/ultralytics/yolov5 
cd yolov5
pip install -r requirements.txt
cd ..


### Run Inference
**Testing/ Inference arguments**  
`--hide-labels` is used to hide the labels in the detected images.  
`--hide-conf` is used to hide the confidence scores in the detected images.  
`--classes 0, 1`, etc used to detect only the classes mentioned here. For our use case we need only signature class, so use `--classes 1`.  
`--line-thickness` integer used to set the thickness of bounding box.  
`--save-crop` and `--save-txt` used to save the crops and labels.  
`--project` could be used to specify the results path  
  
**To test/run inference on a directory of images.**  
> **!python yolov5/detect.py --source yolo_model/tobacco_yolo_format/images/valid/ --weights 'runs/train/Tobacco-run/weights/best.pt' --hide-labels --hide-conf --classes 1 --line-thickness 2**

python3 yolov5/detect.py --source yolo_model/tobacco_yolo_format/images/inputs/ --weights 'trained_model/yolo_model/best.pt' --hide-labels --hide-conf --classes 1 --line-thickness 2


**To pedict a single image**  
> **!python yolov5/detect.py --source tobacco_yolo_format/images/valid/imagename --weights 'runs/train/Tobacco-run/weights/best.pt' --hide-labels --hide-conf --classes 1 --line-thickness 2**  


